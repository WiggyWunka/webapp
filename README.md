# Web App #

## What is this? ##
* it's a learning tool to better understand how a web app would work.
* it's based off of one of my own apps [Underground Biology](https://play.google.com/store/apps/details?id=com.undergroundbiology.undergroundbiology)
* it's meant to help those who need a little clarification on what they're doing.

## FAQ ##

### Can I take the code and use it for my own app? ###
If you give credit then sure. If you don't want to give credit, then please contact me and at least let me know that I helped you. (I won't tell nobody ;D )

### THIS CODE DOESN'T WORK? WTF? ###
Well...yeah. it's not gonna work right off the bat. You'd have to add a few files to get it up and running. If you need help with that, feel free to contact me! I'd be happy to help.

### Why should I use your code instead of writing my own? ###
That's a good question.